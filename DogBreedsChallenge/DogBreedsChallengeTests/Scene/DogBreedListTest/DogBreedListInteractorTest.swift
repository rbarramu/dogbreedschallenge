//
//  DogBreedListInteractorTest.swift
//  DogBreedsChallengeTests
//
//  Created by Rocio Barramuño on 30-11-20.
//
@testable import DogBreedsChallenge
import XCTest

class DogBreedListInteractorTest: XCTestCase {
    // MARK: - Subject under test

    var sut: DogBreedListInteractor!

    // MARK: - Test lifecycle

    override func setUp() {
        super.setUp()
        sut = DogBreedListInteractor()
    }

    override func tearDown() {
        super.tearDown()
    }

    class DogBreedListPresentationLogicSpy: DogBreedListPresentationLogic {
        // MARK: Method call expectations

        var presentDogImageListCalled = false
        var displayLoadingHudCalled = false
        var dismissLoadingHudCalled = false
        var displayErrorCalled = false
        var displaySearchCalled = false
        var presentShowBreedsCalled = false

        // MARK: Spied methods

        func displayLoadingHud() {
            displayLoadingHudCalled = true
        }

        func dismissLoadingHud() {
            dismissLoadingHudCalled = true
        }

        func displayError(error _: AlertViewTypes) {
            displayErrorCalled = true
        }

        func displaySearch() {
            displaySearchCalled = true
        }

        func presentShowBreeds(response _: DogBreedList.Breeds.Response) {
            presentShowBreedsCalled = true
        }

        func presentDogImageList() {
            presentDogImageListCalled = true
        }
    }

    class DogBreedListWorkerSpy: DogBreedListWorker {
        // MARK: Method call expectations

        var showBreedsCalled = false

        // MARK: Spied methods

        override func showBreeds(completionHandler: @escaping ([Breed], AlertViewTypes?) -> Void) {
            showBreedsCalled = true
            completionHandler(DogBreed.all, nil)
        }
    }

    // MARK: Tests

    func testShowBreedsShouldAskPresenterToReturnResult() {
        // Given
        let dogBreedListPresentationLogicSpy = DogBreedListPresentationLogicSpy()
        sut.presenter = dogBreedListPresentationLogicSpy
        let dogBreedListWorkerSpy = DogBreedListWorkerSpy(breedsStore: BreedsAPI())
        sut.worker = dogBreedListWorkerSpy

        XCTAssertEqual(dogBreedListWorkerSpy.showBreedsCalled, false)
        XCTAssertEqual(dogBreedListPresentationLogicSpy.presentShowBreedsCalled, false)

        // When
        let request = DogBreedList.Breeds.Request()
        sut.showDogBreedList(request: request)

        // Then

        XCTAssertEqual(dogBreedListWorkerSpy.showBreedsCalled, true)
        XCTAssertEqual(dogBreedListPresentationLogicSpy.presentShowBreedsCalled, true)
    }
}
