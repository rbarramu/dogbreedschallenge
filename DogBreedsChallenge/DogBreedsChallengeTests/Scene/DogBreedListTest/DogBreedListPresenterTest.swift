//
//  DogBreedListPresenterTest.swift
//  DogBreedsChallengeTests
//
//  Created by Rocio Barramuño on 30-11-20.
//

@testable import DogBreedsChallenge
import XCTest

class DogBreedListPresenterTest: XCTestCase {
    // MARK: - Subject under test

    var sut: DogBreedListPresenter!

    // MARK: Test lifecycle

    override func setUp() {
        super.setUp()
        sut = DogBreedListPresenter()
    }

    override func tearDown() {
        super.tearDown()
    }

    class DogBreedListDisplayLogicSpy: DogBreedListDisplayLogic {
        // MARK: Method call expectations

        var displayDogBreedListCalled = false
        var displayLoadingHudCalled = false
        var dismissLoadingHudCalled = false
        var displayErrorCalled = false
        var displaysearchBreedListCalled = false
        var showDogImageListCalled = false

        // MARK: Argument expectations

        var viewModel: DogBreedList.Breeds.ViewModel!

        // MARK: Spied Methods

        func displayDogBreedList(viewModel: DogBreedList.Breeds.ViewModel) {
            displayDogBreedListCalled = true
            self.viewModel = viewModel
        }

        func displayLoadingHud() {
            displayLoadingHudCalled = true
        }

        func dismissLoadingHud() {
            dismissLoadingHudCalled = true
        }

        func displayError(error _: AlertViewTypes) {
            displayErrorCalled = true
        }

        func displaysearchBreedList() {
            displaysearchBreedListCalled = true
        }

        func showDogImageList() {
            showDogImageListCalled = true
        }
    }

    // MARK: Tests

    func testPresentShowBreedsShouldFormatForDisplay() {
        // Given
        let dogBreedListDisplayLogicSpy = DogBreedListDisplayLogicSpy()
        sut.viewController = dogBreedListDisplayLogicSpy

        // When
        let breed = DogBreed.chihuahua
        let response = DogBreedList.Breeds.Response(breeds: [breed])
        sut.presentShowBreeds(response: response)

        // Then
        let breeds = dogBreedListDisplayLogicSpy.viewModel.breeds
        for breed in breeds {
            XCTAssertEqual(breed.name, "Chihuahua")
        }
    }

    func testPresentShowBreedsShouldAskViewControllerToDisplay() {
        // Given
        let dogBreedListDisplayLogicSpy = DogBreedListDisplayLogicSpy()
        sut.viewController = dogBreedListDisplayLogicSpy

        // When
        let response = DogBreedList.Breeds.Response(breeds: [])
        sut.presentShowBreeds(response: response)

        // Then
        XCTAssert(dogBreedListDisplayLogicSpy.displayDogBreedListCalled, "")
    }
}
