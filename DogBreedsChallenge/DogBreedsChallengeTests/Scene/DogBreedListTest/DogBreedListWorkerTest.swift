//
//  DogBreedListWorkerTest.swift
//  DogBreedsChallengeTests
//
//  Created by Rocio Barramuño on 30-11-20.
//

@testable import DogBreedsChallenge
import XCTest

class DogBreedListWorkerTest: XCTestCase {
    // MARK: Subject under test

    var sut: DogBreedListWorker!
    static var testBreeds: [Breed]!

    // MARK: Test lifecycle

    override func setUp() {
        super.setUp()
        sut = DogBreedListWorker(breedsStore: BreedsAPISpy())
        DogBreedListWorkerTest.testBreeds = DogBreed.all
    }

    override func tearDown() {
        super.tearDown()
    }

    class BreedsAPISpy: BreedsAPI {
        // MARK: Method call expectations

        var showBreedsCalled = false

        // MARK: Spied methods

        override func showBreeds(completionHandler: @escaping ([Breed], AlertViewTypes?) -> Void) {
            showBreedsCalled = true
            completionHandler(DogBreedListWorkerTest.testBreeds, nil)
        }
    }

    // MARK: Tests

    func testShowOrdersShouldReturnListOfOrders() {
        // Given
        let breedsAPISpy = sut.breedsStore as! BreedsAPISpy

        // When
        var showBreeds = [Breed]()
        let expect = expectation(description: "Wait for showBreeds() to return")
        sut.showBreeds { breeds, _ in
            showBreeds = breeds
            expect.fulfill()
        }
        waitForExpectations(timeout: 1.1)

        // Then
        XCTAssert(breedsAPISpy.showBreedsCalled, "")
        XCTAssertEqual(showBreeds.count, DogBreedListWorkerTest.testBreeds.count, "")
        for breed in showBreeds {
            XCTAssert(DogBreedListWorkerTest.testBreeds.contains(breed), "")
        }
    }
}
