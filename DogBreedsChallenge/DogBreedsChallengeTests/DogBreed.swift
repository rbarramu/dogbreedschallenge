//
//  DogBreed.swift
//  DogBreedsChallengeTests
//
//  Created by Rocio Barramuño on 30-11-20.
//
@testable import DogBreedsChallenge
struct DogBreed {
    static let maltese = Breed.createBreedTest(name: "Maltese")
    static let chihuahua = Breed.createBreedTest(name: "Chihuahua")
    static let all = [maltese, chihuahua]
}
