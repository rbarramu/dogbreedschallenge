//
//  Constants.swift
//  DogBreedsChallenge
//
//  Created by Rocio Barramuño on 29-11-20.
//

import Foundation
import UIKit

enum AlertViewTypes {
    case serverError
    case noInternetAlert
}

struct Constants {
    struct Images {
        static let pawIcon = UIImage(named: "pawIcon")
        static let placeholderImage = UIImage(named: "placeholderImage")
    }

    struct Colors {
        static let greenNavBarColor = UIColor(red: 131 / 255, green: 217 / 255, blue: 174 / 255, alpha: 1)
        static let blackLoadingHubColor = UIColor(red: 0.16, green: 0.17, blue: 0.21, alpha: 1)
    }
}

struct Messages {
    struct UIAlertView {
        static let internetText = NSLocalizedString("Sin conexión a Internet.", comment: "")
        static let serverErrorText = NSLocalizedString("Lo sentimos ha ocurrido un error con el servidor.", comment: "")
        static let okText = NSLocalizedString("OK", comment: "")
    }

    struct LoadingHub {
        static let loadingText = NSLocalizedString("Loading", comment: "")
    }

    struct SearchBar {
        static let searchText = NSLocalizedString("Search Dog breeds...", comment: "")
    }

    struct NavBar {
        static let navBarTitleText = NSLocalizedString("Dog Breed List", comment: "")
    }
}

struct Api {
    struct Server {
        static let debugURL = true

        /// URL base.
        static let baseURL = "https://dog.ceo/"
        static let version = "api/"
        static let breeds = "breeds/list"
        static let images = "breed/%@/images"
    }
}
