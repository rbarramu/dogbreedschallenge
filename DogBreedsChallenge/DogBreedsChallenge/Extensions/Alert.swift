//
//  Alert.swift
//  DogBreedsChallenge
//
//  Created by Rocio Barramuño on 29-11-20.
//

import Foundation
import UIKit

extension UIViewController {
    // alert
    func presentAlertView(alertViewType type: AlertViewTypes) {
        var alertTitle: String = ""
        var alertMessage: String = ""
        var acceptTitle: String = ""

        switch type {
        case .serverError:
            alertTitle = ""
            alertMessage = Messages.UIAlertView.serverErrorText
            acceptTitle = Messages.UIAlertView.okText
        case .noInternetAlert:
            alertTitle = ""
            alertMessage = Messages.UIAlertView.internetText
            acceptTitle = Messages.UIAlertView.okText
        }

        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: acceptTitle, style: .default, handler: nil))

        present(alertController, animated: true, completion: nil)
    }
}
