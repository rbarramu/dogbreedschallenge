//
//  BreedsAPI.swift
//  DogBreedsChallenge
//
//  Created by Rocio Barramuño on 29-11-20.
//

import Alamofire
import Foundation

class BreedsAPI: BreedsStoreProtocol {
    // MARK: - operations - Optional error

    func showBreeds(completionHandler: @escaping ([Breed], AlertViewTypes?) -> Void) {
        guard NetworkReachabilityManager()!.isReachable else { return completionHandler([], .noInternetAlert) }

        // creating a NSURL
        let apiUrl = Api.Server.baseURL + Api.Server.version + Api.Server.breeds

        guard let url = URL(string: apiUrl) else {
            completionHandler([], .serverError)
            return
        }

        AF.request(url,
                   method: .get,
                   parameters: nil)
            .validate()
            .responseJSON { response in
                switch response.result {
                case let .success(value):
                    let jsonValue = value as! NSDictionary
                    var breedsDictionary = [Breed]()
                    let breeds = jsonValue["message"] as? NSArray ?? []
                    for breed in breeds {
                        let breedDog = Breed()
                        breedDog.name = breed as! String
                        breedsDictionary.append(breedDog)
                    }
                    completionHandler(breedsDictionary, nil)
                case .failure:
                    completionHandler([], .serverError)
                }
            }
    }

    func showDogBreedImages(forBreed breedName: String,
                            completionHandler: @escaping ([String], AlertViewTypes?) -> Void) {
        guard NetworkReachabilityManager()!.isReachable else { return completionHandler([], .noInternetAlert) }

        let fullString = String(
            format: Api.Server.images,
            breedName
        )

        // Creating a NSURL
        let apiUrl = Api.Server.baseURL + Api.Server.version + fullString

        guard let url = URL(string: apiUrl) else {
            completionHandler([], .serverError)
            return
        }

        AF.request(url,
                   method: .get,
                   parameters: nil)
            .validate()
            .responseJSON { response in
                switch response.result {
                case let .success(value):
                    let jsonValue = value as! NSDictionary
                    var breedsObjectArray: [String] = []
                    let breedArray = jsonValue["message"] as? NSArray ?? []
                    for breedURLs in breedArray {
                        breedsObjectArray.append(breedURLs as! String)
                    }

                    completionHandler(breedsObjectArray, nil)
                case .failure:
                    completionHandler([], .serverError)
                }
            }
    }
}
