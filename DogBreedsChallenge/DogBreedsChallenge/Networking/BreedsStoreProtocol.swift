//
//  BreedsStoreProtocol.swift
//  DogBreedsChallenge
//
//  Created by Rocio Barramuño on 29-11-20.
//

import Foundation

// MARK: - Breeds store API

protocol BreedsStoreProtocol {
    func showBreeds(completionHandler: @escaping ([Breed], AlertViewTypes?) -> Void)
    func showDogBreedImages(forBreed breedName: String,
                            completionHandler: @escaping ([String], AlertViewTypes?) -> Void)
}
