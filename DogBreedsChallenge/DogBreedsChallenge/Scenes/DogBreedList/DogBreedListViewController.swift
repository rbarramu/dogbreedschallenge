//
//  DogBreedListViewController.swift
//  DogBreedsChallenge
//
//  Created by Rocio Barramuño on 29-11-20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol DogBreedListDisplayLogic: class {
    func displayDogBreedList(viewModel: DogBreedList.Breeds.ViewModel)
    func showDogImageList()
    func displaysearchBreedList()
    func displayLoadingHud()
    func dismissLoadingHud()
    func displayError(error: AlertViewTypes)
}

class DogBreedListViewController: UIViewController, DogBreedListDisplayLogic {
    var interactor: DogBreedListBusinessLogic?
    var router: (NSObjectProtocol & DogBreedListRoutingLogic & DogBreedListDataPassing)?

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup

    private func setup() {
        let viewController = self
        let interactor = DogBreedListInteractor()
        let presenter = DogBreedListPresenter()
        let router = DogBreedListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: Routing

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Constants.Colors.greenNavBarColor
        configureNavigationBar()
        configureTableView()
        configureSearchBar()
        showDogBreedList()
    }

    override func viewDidAppear(_: Bool) {
        interactor?.cancelSearchDogBreed()
    }

    // MARK: Show Dog Breed List

    let dogListTableView = UITableView()
    let search = UISearchController(searchResultsController: nil)
    var loadingHud = UIActivityIndicatorView()
    var refreshControl = UIRefreshControl()

    // Create and configure UITableView() programatically
    func configureTableView() {
        view.addSubview(dogListTableView)

        // Constraints
        dogListTableView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            dogListTableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 60),
            dogListTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            dogListTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            dogListTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])

        dogListTableView.dataSource = self
        dogListTableView.delegate = self
        dogListTableView.separatorStyle = .singleLine
        dogListTableView.showsVerticalScrollIndicator = false
        dogListTableView.backgroundColor = .white
        dogListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "BreedCell")

        // Refresh table view
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        dogListTableView.addSubview(refreshControl)
    }

    // Configure navBar programatically
    func configureNavigationBar() {
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.largeTitleTextAttributes = textAttributes
        navigationController?.navigationBar.backgroundColor = Constants.Colors.greenNavBarColor
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = Constants.Colors.greenNavBarColor
        title = Messages.NavBar.navBarTitleText
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }

    // Create and configure SearchBar programatically
    func configureSearchBar() {
        search.searchBar.searchBarStyle = UISearchBar.Style.minimal
        search.searchBar.placeholder = Messages.SearchBar.searchText
        search.searchBar.delegate = self
        search.searchBar.searchTextField.clearButtonMode = .never
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController = search
        search.obscuresBackgroundDuringPresentation = false
    }

    func showDogBreedList() {
        let request = DogBreedList.Breeds.Request()
        interactor?.showDogBreedList(request: request)
    }

    func displayDogBreedList(viewModel _: DogBreedList.Breeds.ViewModel) {
        dogListTableView.reloadData()
    }

    // route to the next view
    func showDogImageList() {
        router?.routeToDogImageList()
    }

    func displaysearchBreedList() {
        dogListTableView.reloadData()
    }

    // Code to refresh table view
    @objc func refresh(_: AnyObject) {
        refreshControl.endRefreshing()
        // Clear any search criteria
        search.searchBar.text = ""
        let request = DogBreedList.Breeds.Request()
        interactor?.showDogBreedList(request: request)
    }

    // MARK: - UI Updates

    func displayLoadingHud() {
        // Show loading view
        view.isUserInteractionEnabled = false
        DispatchQueue.main.async
        {
            self.loadingHud.style = .large
            self.loadingHud.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            self.loadingHud.backgroundColor = Constants.Colors.blackLoadingHubColor
            self.loadingHud.color = .white
            let titleLabel = UILabel(frame: CGRect(x: 10,
                                                   y: self.loadingHud.frame.height - 30,
                                                   width: self.loadingHud.frame.width - 20,
                                                   height: 20))
            titleLabel.text = Messages.LoadingHub.loadingText
            titleLabel.adjustsFontSizeToFitWidth = true
            titleLabel.textAlignment = NSTextAlignment.center
            titleLabel.textColor = .white
            self.loadingHud.layer.cornerRadius = 6
            self.loadingHud.center = self.view.center
            self.view.addSubview(self.loadingHud)
            self.loadingHud.addSubview(titleLabel)
            self.loadingHud.startAnimating()
        }
    }

    func dismissLoadingHud() {
        // Hide loading view
        view.isUserInteractionEnabled = true

        DispatchQueue.main.async
        {
            self.loadingHud.stopAnimating()
            self.loadingHud.removeFromSuperview()
        }
    }

    func displayError(error: AlertViewTypes) {
        presentAlertView(alertViewType: error)
        dogListTableView.reloadData()
    }
}

// MARK: - UITableViewDataSource

extension DogBreedListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return interactor?.numberOfRows ?? 0
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BreedCell", for: indexPath)
        cell.textLabel?.text = interactor?.cellForRow(at: indexPath.row)
        cell.imageView?.image = Constants.Images.pawIcon
        // Add Constraints to image icon
        cell.imageView?.translatesAutoresizingMaskIntoConstraints = false
        let marginguide = cell.contentView.layoutMarginsGuide
        cell.imageView?.topAnchor.constraint(equalTo: marginguide.topAnchor).isActive = true
        cell.imageView?.leadingAnchor.constraint(equalTo: marginguide.leadingAnchor).isActive = true
        cell.imageView?.heightAnchor.constraint(equalToConstant: 40).isActive = true
        cell.imageView?.widthAnchor.constraint(equalToConstant: 40).isActive = true
        cell.imageView?.contentMode = .scaleAspectFit

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        interactor?.didSelect(at: indexPath.row)
        // Clear any search criteria
        search.searchBar.text = ""
    }
}

// MARK: - UISearchBarDelegate

extension DogBreedListViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_: UISearchBar) {}

    func searchBarSearchButtonClicked(_: UISearchBar) {
        view.endEditing(true)
        interactor?.searchDogBreed(key: search.searchBar.text!)
    }

    func searchBarCancelButtonClicked(_: UISearchBar) {
        interactor?.cancelSearchDogBreed()
    }

    func searchBar(_: UISearchBar, textDidChange keyword: String) {
        interactor?.searchDogBreed(key: keyword)
    }
}
