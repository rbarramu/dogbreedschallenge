//
//  Breed.swift
//  DogBreedsChallenge
//
//  Created by Rocio Barramuño on 29-11-20.
//

import Foundation
import RealmSwift

class Breed: Object {
    @objc dynamic var remoteID = UUID().uuidString
    @objc dynamic var name = ""

    override static func primaryKey() -> String? {
        return "remoteID"
    }
}

// swiftlint:disable all
extension Breed {
    // class used in tests
    class func createBreedTest(name: String) -> Breed {
        let breed = Breed()
        breed.name = name
        return breed
    }

    func save() {
        let realm = try! Realm()
        try! realm.write {
            realm.add(self)
        }
    }

    class func all() -> Results<Breed> {
        let realm = try! Realm()
        return realm.objects(Breed.self)
    }

    class func allArray() -> [Breed] {
        var breeds = [Breed]()
        for breed in Breed.all() {
            breeds.append(breed)
        }
        return breeds
    }

    func delete() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(self)
        }
    }
}

// swiftlint:enable all
