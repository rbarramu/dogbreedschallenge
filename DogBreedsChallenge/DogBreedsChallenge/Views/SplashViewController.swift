//
//  SplashViewController.swift
//  DogBreedsChallenge
//
//  Created by Rocio Barramuño on 28-11-20.
//

import Lottie
import UIKit

class SplashViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Show animation from lottie
        let animation = Animation.named("SplashAnimation")
        let animationView = AnimationView(animation: animation)
        splashAnimation.addSubview(animationView)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.animation = animation
        animationView.play()
        timeWait()
    }

    @IBOutlet var splashAnimation: UIView!

    // Present navigationBar in the next view
    func showDogList() {
        let navigationController = UINavigationController(rootViewController: DogBreedListViewController())
        navigationController.modalPresentationStyle = .fullScreen
        present(navigationController, animated: true, completion: nil)
    }

    // Time delay to go from one view to another
    func timeWait() {
        let when = DispatchTime.now() + 2.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.showDogList()
        }
    }
}
