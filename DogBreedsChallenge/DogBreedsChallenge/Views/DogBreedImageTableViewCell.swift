//
//  DogBreedImageTableViewCell.swift
//  DogBreedsChallenge
//
//  Created by Rocio Barramuño on 30-11-20.
//

import AlamofireImage
import Foundation
import UIKit

class DogBreedImageTableViewCell: UITableViewCell {
    var breedImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        breedImage.frame = breedImage.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }

    func configureCell(urlString: String) {
        breedImage = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))

        let size = breedImage.frame.size
        let placeholderImage = Constants.Images.placeholderImage
        addSubview(breedImage)

        guard let url = URL(string: urlString) else {
            breedImage.image = placeholderImage
            return
        }

        breedImage?.af.setImage(
            withURL: url,
            placeholderImage: placeholderImage,
            filter: AspectScaledToFitSizeFilter(size: size),
            imageTransition: .crossDissolve(0.2)
        )
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        breedImage?.af.cancelImageRequest()
        breedImage?.layer.removeAllAnimations()
        breedImage?.image = nil
    }
}
