# Dog Breeds

[![Swift 5](https://img.shields.io/badge/Swift-5-orange.svg?style=flat)](https://swift.org)
[![Xcode 12.2](https://img.shields.io/badge/Xcode-12.2-orange.svg?style=flat)](https://developer.apple.com/xcode/)
[![CocoaPods](https://img.shields.io/badge/CocoaPods-Compatible-yellow.svg?style=flat)](https://cocoapods.org)
[![License](https://img.shields.io/badge/license-MIT-yellow.svg?style=flat)](https://gitlab.com/rbarramu/dogbreedschallenge/-/blob/master/LICENSE)

A dog breed list App using clean architecture (clean swift) and unit test

![](DogBreedsChallenge/Img/cleanSwift.png)

### Prerequisites

* [Xcode](https://developer.apple.com/xcode/) 12.2
* [Cocoapods](https://cocoapods.org)

### Installing

To run the project, clone the repository

```
https://gitlab.com/rbarramu/dogbreedschallenge
```

Go to root directory and run

```
pod install
```

Open DogBreedChallenge.xcworkspace and build

### Tools

* [Alamofire](https://github.com/Alamofire/Alamofire) - Alamofire is an HTTP networking library written in Swift.
* [AlamofireImage](https://github.com/Alamofire/AlamofireImage) - AlamofireImage is an image component library for Alamofire.
* [Realm](https://realm.io/docs/swift/latest/) - Realm is a mobile database that runs directly inside phones, tablets or wearables.
* [Lottie](https://github.com/airbnb/lottie-ios) - Lottie is a mobile library for Android and iOS that natively renders vector based animations and art in realtime with minimal code.
* [SwiftFormat](https://github.com/nicklockwood/SwiftFormat) - SwiftFormat is a code library and command-line tool for reformatting Swift code on macOS or Linux.
* [SwiftLint](https://github.com/realm/SwiftLint) - A tool to enforce Swift style and conventions.

